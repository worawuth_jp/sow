var appModule = {
  server() {
    return "https://pilowy.myminesite.com";
  },
  dataToJSON(data) {
    return JSON.stringify(data);
  },
  JSON_decode(jsonData) {
    return JSON.parse(jsonData);
  },
};

export default appModule;
