import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from 'axios';
import VueAxios from 'vue-axios';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFontAwesome } from '@fortawesome/free-brands-svg-icons';
import serve from './app';
import vmodal from 'vue-js-modal';
import STYLE from "./css/style.css";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import Donut from 'vue-css-donut-chart';
import 'vue-css-donut-chart/dist/vcdonut.css';

Vue.use(Donut);

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

library.add(faFontAwesome)
   
Vue.use(VueAxios, axios,vmodal)
Vue.use(VueSweetalert2);
Vue.use(STYLE);
Vue.use(serve);
   

Vue.config.productionTip = false;

new Vue({
  serve,
  router,
  render: h => h(App)
}).$mount("#app");
