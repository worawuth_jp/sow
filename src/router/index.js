import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Logout from "../views/Logout.vue";
import User from "../views/user/User.vue";
import AddUser from "../views/user/addUser.vue";
import Sow from "../views/sow/Sow.vue";
import AddSow from "../views/sow/addSow.vue";
import Unit from "../views/unit/unit.vue";
import Block from "../views/unit/block.vue";
import Semen from "../views/semen/semen.vue";
import UserBarcode from "../views/user/barcode.vue";
import Employee from "../views/user/employee.vue";
import Position from "../views/user/position.vue";
import Mating from "../views/mating/mating.vue";
import newMating from "../views/mating/mating2.vue";
import Vaccine from "../views/vaccine/vaccine.vue";
import SowVaccine from "../views/vaccine/sowvaccine.vue";
import Report from "../views/report/report.vue";
import ReportSire from "../views/report/reportSire.vue";
import ReportDam from "../views/report/reportDam.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    meta: {
      Auth: true
    }
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      Auth: false
    }
  },
  {
    path: "/user",
    name: "user",
    component: User,
    meta: {
      Auth: true
    }
  },
  {
    path: "/employee/add",
    name: "addUser",
    component: AddUser,
    meta: {
      Auth: true
    }
  },
  {
    path: "/sow",
    name: "sow",
    component: Sow,
    meta: {
      Auth: true
    }
  },
  {
    path: "/mating",
    name: "mating",
    component: Mating,
    meta: {
      Auth: true
    }
  },
  {
    path: "/newmating",
    name: "newmating",
    component: newMating,
    meta: {
      Auth: true
    }
  },
  
  {
    path: "/vaccine",
    name: "vaccine",
    component: Vaccine,
    meta: {
      Auth: true
    }
  },
  {
    path: "/report",
    name: "report",
    component: Report,
    meta: {
      Auth: true
    }
  },
  {
    path: "/report/sire",
    name: "reportSire",
    component: ReportSire,
    meta: {
      Auth: true
    }
  },
  {
    path: "/report/dam",
    name: "reportDam",
    component: ReportDam,
    meta: {
      Auth: true
    }
  },

  {
    path: "/sowvaccine",
    name: "sowvaccine",
    component: SowVaccine,
    meta: {
      Auth: true
    }
  },

  {
    path: "/sow/add",
    name: "addSow",
    component: AddSow,
    meta: {
      Auth: true
    }
  },
  {
    path: "/logout",
    name: "logout",
    component: Logout
  },
  {
    path: "/unit",
    name: "unit",
    component: Unit
  },
  {
    path: "/unit/block/:id",
    name: "block",
    component: Block
  },
  {
    path: "/semen",
    name: "semen",
    component: Semen
  },
  {
    path: "/employee/qrcode",
    name:"userBarcode",
    component: UserBarcode
  },
  {
    path: "/employee",
    name:"employee",
    component: Employee,
  },
  {
    path: "/position",
    name:"position",
    component: Position,
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to,from,next)=>{
  if(to.matched.some(record => record.meta.Auth)){
    if(localStorage.getItem('user') == null){
      next({
        path: "/login",
        params: {nextUrl: to.fullPath}
      })
    }else{
      next()
    }
  }else{
    next()
  }
});

export default router;
